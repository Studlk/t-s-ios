import RxSwift

protocol SignUpViewModelType: BaseViewModelType {
    var signUpAction: PublishSubject<Void> { get }
    var signUpRequestAction: PublishSubject<Void> { get }
    var isButtonEnabled: Observable<Bool> { get }
    var selectedSegmentIndex: BehaviorSubject<Int> { get }
}

final class SignUpViewModel: BaseViewModel, SignUpViewModelType {
    
    var signUpAction = PublishSubject<Void>()
    var signUpRequestAction = PublishSubject<Void>()
    
    var isButtonEnabled: Observable<Bool> = .empty()
    var selectedSegmentIndex = BehaviorSubject<Int>(value: 0)
    
}
