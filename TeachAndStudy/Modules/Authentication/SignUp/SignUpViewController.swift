import UIKit

final class SignUpViewController: BaseViewController {
    
    @IBOutlet private var signUpButton: Button!
    @IBOutlet private var signUpSegmentedControl: UISegmentedControl!
    
    var viewModel: SignUpViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)
        
        bind()
    }
    
    private func bind() {
        signUpSegmentedControl.rx
            .value
            .bind(to: viewModel.selectedSegmentIndex)
            .disposed(by: bag)
        
        signUpButton.rx
            .tap
            .bind(to: viewModel.signUpAction)
            .disposed(by: bag)
        
        //For test
        signUpButton.rx
            .tap
            .bind(to: viewModel.signUpRequestAction)
            .disposed(by: bag)
        //
        
        viewModel.isButtonEnabled
            .bind(to: signUpButton.rx.isEnabled)
            .disposed(by: bag)
    }
}
