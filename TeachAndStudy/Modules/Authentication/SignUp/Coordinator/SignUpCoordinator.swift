import RxSwift

final class SignUpCoordinator: BaseCoordinator<Void> {
    
    override func start() -> Observable<Void> {
        let viewModel = SignUpViewModel()
        let viewController = SignUpViewController()
        
        viewController.viewModel = viewModel
        
        open(viewController)
        
        viewModel.signUpRequestAction
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self, unowned viewController] _ -> Observable<Void> in
                return self?.coordinate(to: VerificationCoordinator(basedOn: viewController, transition: .modal(modalPresentationStyle: .formSheet))) ?? .never()
            }
            .subscribe()
            .disposed(by: bag)
        
        return .never()
    }
}
