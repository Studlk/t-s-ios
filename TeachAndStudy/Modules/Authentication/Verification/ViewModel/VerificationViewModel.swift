import RxSwift

protocol VerificationViewModelType: BaseViewModelType {
    var code: BehaviorSubject<String> { get  set }
    
    var saveAction: PublishSubject<Void> { get }
    var saveRequestAction: PublishSubject<Void> { get }
    var isButtonEnabled: Observable<Bool> { get }
}

final class VerificationViewModel: BaseViewModel, VerificationViewModelType {
    
    var code = BehaviorSubject<String>(value: "")
    
    var saveAction = PublishSubject<Void>()
    var saveRequestAction = PublishSubject<Void>()
    var isButtonEnabled: Observable<Bool> = .empty()
}
