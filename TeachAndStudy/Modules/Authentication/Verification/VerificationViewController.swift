import UIKit

final class VerificationViewController: BaseViewController {
    
    @IBOutlet private var codeTextField: UITextField!
    @IBOutlet private var saveButton: Button!
    
    var viewModel: VerificationViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)
        
        bind()
    }
    
    private func bind() {
        codeTextField.rx
            .text
            .orEmpty
            .bind(to: viewModel.code)
            .disposed(by: bag)
        
        saveButton.rx
            .tap
            .bind(to: viewModel.saveAction)
            .disposed(by: bag)
        
        //For test
        saveButton.rx
            .tap
            .bind(to: viewModel.saveRequestAction)
            .disposed(by: bag)
        //
        
        viewModel.isButtonEnabled
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: bag)
    }
}
