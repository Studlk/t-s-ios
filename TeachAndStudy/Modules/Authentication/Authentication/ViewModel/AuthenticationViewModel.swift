import RxSwift

protocol AuthenticationViewModelType: BaseViewModelType {
    var signInAction: PublishSubject<Void> { get }
    var signUpAction: PublishSubject<Void> { get }
    var skipAction: PublishSubject<Void> { get }
}

final class AuthenticationViewModel: BaseViewModel, AuthenticationViewModelType {
    var signInAction = PublishSubject<Void>()
    let signUpAction = PublishSubject<Void>()
    let skipAction = PublishSubject<Void>()
    
    
    override init() {
        
    }
}
