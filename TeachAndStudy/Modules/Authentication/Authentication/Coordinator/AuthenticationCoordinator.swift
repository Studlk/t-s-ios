import RxSwift

final class AuthenticationCoordinator: BaseCoordinator<Void> {
    
    override func start() -> Observable<Void> {
        let viewModel = AuthenticationViewModel()
        let viewController = AuthenticationViewController()
        
        viewController.viewModel = viewModel
        
        open(viewController)
        
        viewModel.signUpAction
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self, unowned viewController] _ -> Observable<Void> in
                return self?.coordinate(to: SignUpCoordinator(basedOn: viewController, transition: .push)) ?? .never()
            }
            .subscribe()
            .disposed(by: bag)
        
        viewModel.signInAction
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self, unowned viewController] _ -> Observable<Void> in
                return self?.coordinate(to: SignInCoordinator(basedOn: viewController, transition: .push)) ?? .never()
            }
            .subscribe()
            .disposed(by: bag)
        
        viewModel.skipAction
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self, unowned viewController] _ -> Observable<Void> in
                return self?.coordinate(to: TabBarCoordinator(basedOn: viewController, transition: .root)) ?? .empty()
            }
            .subscribe()
            .disposed(by: bag)
        
        return .never()
    }
}
