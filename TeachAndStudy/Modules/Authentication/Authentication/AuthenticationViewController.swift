import UIKit

final class AuthenticationViewController: BaseViewController {
    
    @IBOutlet private var signInButton: Button!
    @IBOutlet private var signUpButton: Button!
    @IBOutlet private var skipButton: Button!
    
    var viewModel: AuthenticationViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)
        viewModel.map { self.bind(with: $0) }
    }
    
    private func bind(with viewModel: AuthenticationViewModelType) {
        signInButton.rx.tap
            .bind(to: viewModel.signInAction)
            .disposed(by: bag)
        
        signUpButton.rx.tap
            .bind(to: viewModel.signUpAction)
            .disposed(by: bag)
        
        skipButton.rx.tap
            .bind(to: viewModel.skipAction)
            .disposed(by: bag)
    }
}
