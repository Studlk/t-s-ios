import RxSwift

final class SignInCoordinator: BaseCoordinator<Void> {
    
    override func start() -> Observable<Void> {
        let viewModel = SignInViewModel()
        let viewController = SignInViewController()
        
        viewController.viewModel = viewModel
        
        
        
        open(viewController)
        
        return .never()
    }
}
