import RxSwift

protocol SignInViewModelType: BaseViewModelType {
    var email: BehaviorSubject<String> { get  set }
    var password: BehaviorSubject<String> { get  set }
    
    var signInAction: PublishSubject<Void> { get }
    var isButtonEnabled: Observable<Bool> { get }
}

final class SignInViewModel: BaseViewModel, SignInViewModelType {
    
    var email = BehaviorSubject<String>(value: "")
    var password = BehaviorSubject<String>(value: "")
    
    var signInAction = PublishSubject<Void>()
    
    private let validator: ValidatorType = Validator()
    
    var isButtonEnabled: Observable<Bool>  {
        return Observable.combineLatest(email, password)
            .flatMapLatest { [weak self] email, password -> Observable<Bool> in
                guard let `self` = self else { return .empty() }
                return .just(self.validator.isValidEmail(email) && self.validator.isValidPassword(password))
            }
    }
    
}
