import UIKit

final class SignInViewController: BaseViewController {
    
    @IBOutlet private var signInButton: Button!
    @IBOutlet private var emailField: UITextField!
    @IBOutlet private var passwordField: UITextField!
    
    var viewModel: SignInViewModelType!

    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)
        
        bind()
    }
    
    private func bind() {
        emailField.rx
            .text
            .orEmpty
            .bind(to: viewModel.email)
            .disposed(by: bag)
        
        passwordField.rx
            .text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: bag)
        
        signInButton.rx
            .tap
            .bind(to: viewModel.signInAction)
            .disposed(by: bag)
        
        viewModel.isButtonEnabled
            .bind(to: signInButton.rx.isEnabled)
            .disposed(by: bag)
    }
}
