import UIKit

final class PasswordViewController: BaseViewController {
    
    @IBOutlet private var passwordTextField: UITextField!
    @IBOutlet private var confirmPasswordTextField: UITextField!
    @IBOutlet private var saveButton: Button!
    
    var viewModel: PasswordViewModelType!
    
    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)

        bind()
    }
    
    private func bind() {
        passwordTextField.rx
            .text
            .orEmpty
            .bind(to: viewModel.password)
            .disposed(by: bag)
        
        confirmPasswordTextField.rx
            .text
            .orEmpty
            .bind(to: viewModel.confirmPassword)
            .disposed(by: bag)
        
        saveButton.rx
            .tap
            .bind(to: viewModel.saveAction)
            .disposed(by: bag)
        
        viewModel.isButtonEnabled
            .bind(to: saveButton.rx.isEnabled)
            .disposed(by: bag)
    }
}
