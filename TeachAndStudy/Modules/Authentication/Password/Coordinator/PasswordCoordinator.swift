import RxSwift

final class PasswordCoordinator: BaseCoordinator<Void> {
    
    override func start() -> Observable<Void> {
        let viewModel = PasswordViewModel()
        let viewController = PasswordViewController()
        
        viewController.viewModel = viewModel
        
        open(viewController)
        
        viewModel.saveRequestAction
            .observeOn(MainScheduler.instance)
            .flatMapLatest { [weak self, unowned viewController] _ -> Observable<Void> in
                return self?.coordinate(to: PasswordCoordinator(basedOn: viewController, transition: .push)) ?? .never()
            }
            .subscribe()
            .disposed(by: bag)
        
        return .never()
    }
}
