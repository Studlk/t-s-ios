import RxSwift

protocol PasswordViewModelType: BaseViewModelType {
    var password: BehaviorSubject<String> { get  set }
    var confirmPassword: BehaviorSubject<String> { get  set }
    
    var saveAction: PublishSubject<Void> { get }
    var saveRequestAction: PublishSubject<Void> { get }
    var isButtonEnabled: Observable<Bool> { get }
}

final class PasswordViewModel: BaseViewModel, PasswordViewModelType {
    
    var password = BehaviorSubject<String>(value: "")
    var confirmPassword = BehaviorSubject<String>(value: "")
    
    var saveAction = PublishSubject<Void>()
    var saveRequestAction = PublishSubject<Void>()
    
    private let validator: ValidatorType = Validator()
    
    var isButtonEnabled: Observable<Bool>  {
        return Observable.combineLatest(password, confirmPassword)
            .flatMapLatest { [weak self] password, confirmPassword -> Observable<Bool> in
                guard let `self` = self else { return .empty() }
                return .just(self.validator.isValidPassword(password) && self.validator.isValidPassword(confirmPassword))
            }
    }
}
