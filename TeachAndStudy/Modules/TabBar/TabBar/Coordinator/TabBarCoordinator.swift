import RxSwift

class TabBarCoordinator: BaseCoordinator<Void> {
    
    var navigationController: UINavigationController!
    
    override func start() -> Observable<Void> {
        
        let tabBarController = TabBarViewController()
        navigationController = tabBarController.embeddedInNavigation()
        navigationController.navigationBar.isHidden = true
        
        let settingsNavigationController = UINavigationController()
        settingsNavigationController.tabBarItem = UITabBarItem(title: "tab_bar_item_profile", image: nil, selectedImage: nil)

        tabBarController.viewControllers = [settingsNavigationController]

        coordinate(to: SettingsCoordinator(window: window, basedOn: settingsNavigationController, transition: .push))
            .subscribe()
            .disposed(by: bag)
        
        open(navigationController)
        
        return .never()
    }
}
