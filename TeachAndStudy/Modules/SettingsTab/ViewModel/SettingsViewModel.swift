import RxSwift

protocol SettingsViewModelType: BaseViewModelType {
    var settings: Observable<[SettingsItems]> { get }
}

final class SettingsViewModel: BaseViewModel, SettingsViewModelType {
    
    private let settingsSubject = BehaviorSubject<[SettingsItems]>(value: SettingsItems.allCases)
    
    var settings: Observable<[SettingsItems]> {
        return settingsSubject
    }
    
    override init() {
        
//        settingsSubject
    }
}
