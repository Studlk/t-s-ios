import UIKit
import RxSwift

final class SettingsViewController: BaseViewController {
    
    @IBOutlet private var tableView: UITableView!
    
    var viewModel: SettingsViewModelType!
    private var settingsList: [SettingsItems] = []
    
    override func viewDidLoad() {
        super.viewDidLoad(with: viewModel)
        
        setupTableView()
        bind()
    }
    
    private func setupTableView() {
        tableView.register(PushNotificationTableViewCell.self)
        tableView.register(SettingsTableViewCell.self)
    }
    
    private func bind() {
        viewModel
            .settings
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] settings in
                        guard let self = self else { return }
                self.settingsList = settings
                self.tableView.reloadData()
            })
            .disposed(by: bag)
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        switch settingsList[indexPath.row] {
        case .pushNotification:
            break
        case .language:
            break
        case .rateApp:
            break
        case .feadback:
            break
        case .privicyPolicy:
            break
        case .aboutUs:
            break
        }
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = settingsList[indexPath.row]
        switch item {
        case .pushNotification:
            let cell = tableView.dequeue(PushNotificationTableViewCell.self, for: indexPath)
            
            item.title
                .asObservable()
                .bind(to: cell.titleLabel.rx.text)
                .disposed(by: bag)
            
            return cell
        case .language, .rateApp,
             .feadback, .privicyPolicy,
             .aboutUs:
            let cell = tableView.dequeue(SettingsTableViewCell.self, for: indexPath)
            
            item.title
                .asObservable()
                .bind(to: cell.titleLabel.rx.text)
                .disposed(by: bag)
            
            return cell
        }
    }
}
