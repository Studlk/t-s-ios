import RxSwift

final class SettingsCoordinator: BaseCoordinator<Void> {
    
    override func start() -> Observable<Void> {
        let viewModel = SettingsViewModel()
        let viewController = SettingsViewController()
        
        viewController.viewModel = viewModel
        
        open(viewController)
        
        
        
        return .never()
    }
}
