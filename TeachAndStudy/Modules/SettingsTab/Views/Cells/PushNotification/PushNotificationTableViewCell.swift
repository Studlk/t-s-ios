import UIKit

protocol PushNotificationTableViewCellProtocol {
    
}

final class PushNotificationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        titleLabel.text = nil
    }
}

extension PushNotificationTableViewCell: PushNotificationTableViewCellProtocol {
    
}
