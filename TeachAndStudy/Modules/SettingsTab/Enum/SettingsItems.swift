import RxSwift

enum SettingsItems: Int, CaseIterable {
    case pushNotification = 0
    case language
    case rateApp
    case feadback
    case privicyPolicy
    case aboutUs
    
    var title: BehaviorSubject<String> {
        switch self {
        case .pushNotification:
            return BehaviorSubject<String>(value: "Push Notification")
        case .language:
            return BehaviorSubject<String>(value: "Language")
        case .rateApp:
            return BehaviorSubject<String>(value: "Rate App")
        case .feadback:
            return BehaviorSubject<String>(value: "Feadback")
        case .privicyPolicy:
            return BehaviorSubject<String>(value: "Privicy Policy")
        case .aboutUs:
            return BehaviorSubject<String>(value: "About us")
        }
    }
}
