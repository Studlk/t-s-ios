import Foundation

protocol ValidatorType: class {
    func isValidEmail(_ email: String) -> Bool
    func isValidPassword(_ password: String) -> Bool
}

final class Validator: ValidatorType {
    
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: email)
    }
    
    /*
     ^                        - Start Anchor.
     (?=.*[a-z])              - Ensure string has one character.
     (?=.[$@$#!%?&])          - Ensure string has one special character.
     {6,}                     - Ensure password length is 6.
     $                        - End Anchor.
     */
    func isValidPassword(_ password: String) -> Bool {
        let passwordRegEx = "^(?=.*[a-z])[A-Za-z\\d$@$#!%*?&]{6,}"
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", passwordRegEx)
        return passwordTest.evaluate(with: password)
    }
}
