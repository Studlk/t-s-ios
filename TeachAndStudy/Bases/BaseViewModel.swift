import Foundation
import RxSwift
import RxSwiftUtilities

public protocol ActivityTrackable {
    var loading: ActivityIndicator { get }
}

public protocol ErrorTrackable {
    var errorTracker: RxErrorTracker { get }
    var errorData: Observable<ErrorAlertData> { get }
}

public protocol AlertDataTrackable {
    var alertData: PublishSubject<AlertData> { get }
}

public protocol UserCallbackViewModel: ErrorTrackable, ActivityTrackable, AlertDataTrackable { }

public protocol BaseViewModelType: UserCallbackViewModel { }

public struct AlertData {
    let title: String
    let message: String
}

open class BaseViewModel: BaseViewModelType {
    public let loading = ActivityIndicator()
    public let bag = DisposeBag()

    public let errorTracker = RxErrorTracker()

    public var errorData: Observable<ErrorAlertData> {
        return errorTracker.asObservable().asErrorAlertData()
    }

    public let alertData = PublishSubject<AlertData>()
    
    public init() {}
}
