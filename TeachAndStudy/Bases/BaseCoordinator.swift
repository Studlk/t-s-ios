import RxSwift

public enum CoordinatorTransitionType {
    case root
    case modal(modalPresentationStyle: UIModalPresentationStyle)
    case push
}

open class BaseCoordinator<ResultType> {
    typealias CoordinationResult = ResultType

    public let transitionType: CoordinatorTransitionType
    public let rootViewController: UIViewController?
    public let window: UIWindow?

    public init(window: UIWindow? = nil, basedOn rootViewController: UIViewController? = nil, transition: CoordinatorTransitionType = .push) {
        transitionType = transition
        self.rootViewController = rootViewController
        self.window = window
    }

    public let bag = DisposeBag()

    private let identifier = UUID()

    private(set) var childCoordinators = [UUID: Any]()

    private func store<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = coordinator
    }

    private func free<T>(coordinator: BaseCoordinator<T>) {
        childCoordinators[coordinator.identifier] = nil
    }

    public func coordinate<T>(to coordinator: BaseCoordinator<T>) -> Observable<T> {
        store(coordinator: coordinator)
        return coordinator.start()
            .do(onNext: { [weak self] _ in
                self?.free(coordinator: coordinator)
            })
    }
    
    deinit {
        debugPrint("✅ Coordinator is deallocated")
    }

    open func start() -> Observable<ResultType> {
        fatalError("Start method should be implemented.")
    }
    
    var keyWindow: UIWindow? {
        return UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
    }

    public func open(_ nextViewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        switch transitionType {
        case .root:
            if let nextViewController = nextViewController as? UINavigationController {
                (window ?? keyWindow)?.rootViewController = nextViewController
            } else {
                (window ?? keyWindow)?.rootViewController = UINavigationController(rootViewController: nextViewController)
            }
            (window ?? keyWindow)?.makeKeyAndVisible()
        case .push:
            push(nextViewController, animated: animated, completion: completion)
        case .modal(let modalPresentationStyle):
            guard let rootViewController = rootViewController else {
                fatalError("Can't present on nil view controller")
            }
            let navigationController = UINavigationController(rootViewController: nextViewController)
            navigationController.modalPresentationStyle = modalPresentationStyle
            rootViewController.present(navigationController, animated: animated) {
                completion?()
            }
        }
    }

    public func push(_ nextViewController: UIViewController, animated: Bool = true, completion _: (() -> Void)? = nil) {
        if nextViewController is UINavigationController {
            fatalError("Can't push navigation controller")
        }

        if let navigationController = rootViewController as? UINavigationController {
            navigationController.pushViewController(nextViewController, animated: animated)
        } else if let navigationController = rootViewController?.navigationController {
            navigationController.pushViewController(nextViewController, animated: animated)
        }
    }

    public func open(url urlString: String) {
        guard let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) else { return }

        UIApplication.shared.open(url, options: [:])
    }
    
    func close(_ viewController: UIViewController, animated: Bool = true, completion: (() -> Void)? = nil) {
        switch transitionType {
        case .push:
            if let navigationController = viewController.navigationController {
                navigationController.popViewController(animated: animated)
            }
        case .modal(_):
            if let _ = viewController.presentingViewController {
                viewController.dismiss(animated: animated) {
                    completion?()
                }
            }
        case .root:
            if let _ = viewController.presentingViewController {
                viewController.dismiss(animated: animated) {
                    completion?()
                }
            }
            else if let navigationController = viewController.navigationController {
                navigationController.popToRootViewController(animated: animated)
            }
        }
    }
}
