import MBProgressHUD

public class ActivityLoader {
    
    public class func show(view: UIView, animated: Bool = true) {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    public class func dismiss(view: UIView, animated: Bool = true) {
        MBProgressHUD.hide(for: view, animated: animated)
        view.subviews.forEach { view in
            if view is MBProgressHUD {
                view.removeFromSuperview()
                return
            }
        }
    }
}
