import Foundation

public struct PlatformError: Error {
    let reason: String
}
