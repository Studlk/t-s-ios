import Foundation

public struct AppError: Error {
    let reason: String
}

extension AppError {
    static public var unexpectedError: AppError {
        return AppError(reason: "Something went wrong")
    }
}
