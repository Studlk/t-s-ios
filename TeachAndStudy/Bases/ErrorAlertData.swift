import Foundation
import RxSwift

public struct ErrorAlertData {
    let title: String
    let message: String?
    
    public init(title: String, message: String? = nil) {
        self.title = title
        self.message = message
    }
}

extension ObservableType where Element == Error {
    public func asErrorAlertData() -> Observable<ErrorAlertData> {
        return map { error -> ErrorAlertData in
            
            if let platformError = error as? PlatformError {
                return ErrorAlertData(title: platformError.reason)
            }
            
            if let appError = error as? AppError {
                return ErrorAlertData(title: appError.reason)
            }
            
            return ErrorAlertData(title: error.localizedDescription)
        }
    }
}
