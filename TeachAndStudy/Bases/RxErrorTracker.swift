import RxSwift

final public class RxErrorTracker: ObservableConvertibleType {
    private let _subject = PublishSubject<Error>()
    
    deinit {
        _subject.onCompleted()
    }
    
    public func trackError<O: ObservableConvertibleType>(from source: O) -> Observable<O.Element> {
        return source.asObservable().do(onError: onError)
    }
    
    public func asObservable() -> Observable<Error> {
        return _subject.asObservable()
    }
    
    public func onError(_ error: Error) {
        _subject.onNext(error)
    }
}

extension ObservableConvertibleType {
    public func trackError(_ errorTracker: RxErrorTracker) -> Observable<Element> {
        return errorTracker.trackError(from: self).catchError { _ in return .empty() }
    }
}
