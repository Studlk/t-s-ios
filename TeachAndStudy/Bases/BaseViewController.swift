import UIKit
import RxSwift

open class BaseViewController: UIViewController {
    private var baseViewModel: BaseViewModelType?
    
    private(set) var isBusy: Observable<Bool> = .empty()
    
    public let bag = DisposeBag()
    
    public var safeAreaInsets: UIEdgeInsets {
        if #available(iOS 11.0, *) {
            return self.view.safeAreaInsets
        }
        else {
            return UIEdgeInsets(top: self.topLayoutGuide.length, left: 0.0, bottom: self.bottomLayoutGuide.length, right: 0.0)
        }
    }
    
    deinit {
        print("✅ VIEW CONTROLLER IS DEALLOCATED")
    }
    
    var keyWindow: UIWindow? {
        return UIApplication.shared.connectedScenes
            .filter({$0.activationState == .foregroundActive})
            .map({$0 as? UIWindowScene})
            .compactMap({$0})
            .first?.windows
            .filter({$0.isKeyWindow}).first
    }
    
    public func viewDidLoad(with viewModel: BaseViewModelType) {
        self.baseViewModel = viewModel
        
        super.viewDidLoad()
        
//        view.backgroundColor = UIColor.Assets.appBackground
        
        makeTransparentNavigationBar()
        
        isBusy = viewModel.loading.asObservable().distinctUntilChanged()
        
        // Disable buttons when making requests
        let buttons = view.subviews.filter { $0 is UIButton }
        isBusy
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { isBusy in
                buttons.forEach {
                    $0.isUserInteractionEnabled = !isBusy
                }
            })
            .disposed(by: bag)
        
        viewModel.loading
            .asObservable()
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] isLoading in
                guard let self = self else { return }
                if isLoading {
                    ActivityLoader.show(view: self.view)
                } else {
                    ActivityLoader.dismiss(view: self.view)
                }
            })
            .disposed(by: bag)
        
        viewModel.errorData
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.showAlert(title: $0.title, message: $0.message)
            })
            .disposed(by: bag)
        
        viewModel.alertData
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                self?.showAlert(title: $0.title, message: $0.message)
            })
            .disposed(by: bag)
    }
    
    func showActivity(until trigger: Observable<Void>) {
        ActivityLoader.show(view: view)
        
        let buttons = view.subviews.filter { $0 is UIButton }
        buttons.forEach {
            $0.isUserInteractionEnabled = false
        }
        trigger
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [weak self] in
                guard let self = self else { return }
                ActivityLoader.dismiss(view: self.view)
                buttons.forEach {
                    $0.isUserInteractionEnabled = true
                }
            })
            .disposed(by: bag)
    }
    
    func showAlert(title: String, message: String? = nil, actions: [UIAlertAction] = []) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.view.tintColor = .blue
        if actions.isEmpty {
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        } else {
            actions.forEach { alert.addAction($0) }
        }
        self.present(alert, animated: true)
    }
    
    func hideKeyboard() {
        view.endEditing(true)
    }
    
    func occureImpactFeedback(style: UIImpactFeedbackGenerator.FeedbackStyle = .soft) {
        let feedback = UIImpactFeedbackGenerator(style: style)
        feedback.prepare()
        feedback.impactOccurred()
    }
    
    func occureButtonFeedback() {
        occureImpactFeedback(style: .light)
    }
    
    func occureWarningFeedback() {
        occureNotificationFeedback(.warning)
    }
    
    func occureNotificationFeedback(_ notificationType: UINotificationFeedbackGenerator.FeedbackType) {
        let notificationFeedbackGenerator = UINotificationFeedbackGenerator()
        notificationFeedbackGenerator.prepare()
        notificationFeedbackGenerator.notificationOccurred(notificationType)
    }
    
    func makeTransparentNavigationBar() {
        navigationController?.setNavigationBarHidden(false, animated: false)
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.view.backgroundColor = .clear
    }
    
    func open(url urlString: String) {
        guard let url = URL(string: urlString), UIApplication.shared.canOpenURL(url) else { return }

        UIApplication.shared.open(url, options: [:])
    }
    
    func animateUI(completion: ((Bool) -> Void)? = nil) {
        UIView.animate(
            withDuration: 0.3,
            delay: 0,
            usingSpringWithDamping: 2.0,
            initialSpringVelocity: 0.1,
            options: .curveEaseOut,
            animations: {
                self.view.layoutIfNeeded()
            },
            completion: completion
        )
    }
}
