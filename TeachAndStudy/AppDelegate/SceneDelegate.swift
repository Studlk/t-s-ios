import UIKit
import RxSwift

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    private var coordinator: AuthenticationCoordinator!
    private let bag = DisposeBag()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(windowScene: windowScene)
        
        coordinator = AuthenticationCoordinator(window: window, transition: .root)
        coordinator
            .start()
            .subscribe()
            .disposed(by: bag)
    }
}
