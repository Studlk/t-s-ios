import UIKit

extension UIViewController {
    func embeddedInNavigation() -> UINavigationController {
        return UINavigationController(rootViewController: self)
    }
}
